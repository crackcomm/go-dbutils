// Package db implements utility for applications with single mongo session.
package db

import (
	"flag"
	"time"

	"gopkg.in/mgo.v2"
)

// DefaultSession - Mongo session.
var DefaultSession *mgo.Session

// dbURL - Database url.
var dbURL string

// dbTimeout - Database connection timeout.
var dbTimeout = 300 * time.Second

// InitFlags - Initializes flags in default FlagSet.
func InitFlags() {
	flag.StringVar(&dbURL, "mongo-url", "mongodb://127.0.0.1:27017/movies", "MongoDB url")
	flag.DurationVar(&dbTimeout, "mongo-timeout", dbTimeout, "MongoDB connection timeout")
}

// Init - Initializes a database.
func Init() (err error) {
	if !flag.Parsed() {
		flag.Parse()
	}

	DefaultSession, err = mgo.DialWithTimeout(dbURL, dbTimeout)
	return
}

// Session - Returns cloned DefaultSession. It needs to be closed afterwards.
func Session() *mgo.Session {
	return DefaultSession.Clone()
}

// DB - Returns database from cloned DefaultSession. Session needs to be closed.
func DB() *mgo.Database {
	return Session().DB("")
}
